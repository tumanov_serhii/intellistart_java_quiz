package com.example.dbProject.entity;

import com.example.dbProject.dto.ProductCreateDTO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.util.Set;


@Data
@AllArgsConstructor
@NoArgsConstructor

@Entity
@Table(name = "Product")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    @NotEmpty
    private String name;

    @Min(value = 0)
    @NonNull
    @Column(name = "price")
    private Integer price;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            }, mappedBy = "products")
    @JsonIgnore
    private Set<User> users;


    public Product(ProductCreateDTO productCreateDTO) {
        name = productCreateDTO.getName();
        price = productCreateDTO.getPrice();
    }


    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        Product guest = (Product) obj;
        return id.equals(guest.id);
    }

    @Override
    public int hashCode() {
        return id;
    }
}

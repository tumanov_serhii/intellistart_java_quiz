package com.example.dbProject.entity;

import com.example.dbProject.dto.UserCreateDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.util.Set;


@Data
@AllArgsConstructor
@NoArgsConstructor

@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "firstName")
    @NotEmpty
    private String firstName;
    @Column(name = "lastName")
    @NotEmpty
    private String lastName;

    @Column(name = "totalMoney")
    @NonNull
    @Min(value = 0)
    private Integer totalMoney;


    public User(UserCreateDTO userDTO) {
        firstName = userDTO.getFirstName();
        lastName = userDTO.getLastName();
        totalMoney = userDTO.getTotalMoney();
    }

    @ManyToMany
    @JoinTable(
            name = "product_user",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "product_id"))
    private Set<Product> products;


    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }

        User guest = (User) obj;
        return id.equals(guest.id);
    }

    @Override
    public int hashCode() {
        return id;
    }
}

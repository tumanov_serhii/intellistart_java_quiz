package com.example.dbProject.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ProductCreateDTO {
    private String name;
    private Integer price;
}
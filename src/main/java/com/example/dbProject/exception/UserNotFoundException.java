package com.example.dbProject.exception;

public class UserNotFoundException extends RuntimeException {

    public UserNotFoundException(Integer id) {

        super(String.format("User with Id %d not found", id));
    }
}
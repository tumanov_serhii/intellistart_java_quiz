package com.example.dbProject.service;

import com.example.dbProject.dto.UserCreateDTO;
import com.example.dbProject.dto.UserUpdateDTO;
import com.example.dbProject.entity.User;

import java.util.Collection;

public interface UserService {
    Collection<User> getList();

    Integer deleteUser(Integer id);

    Integer createUser(UserCreateDTO user1);

    User getUser(Integer id);

    User updateUser(Integer id, UserUpdateDTO userDTO);

    User updateUserProperty(Integer id, UserUpdateDTO userDTO2);

    Integer deleteProductFromUser(Integer userId, Integer productId);

    Integer addProductToUser(Integer userId, Integer productId);
}


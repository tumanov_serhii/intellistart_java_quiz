package com.example.dbProject.service;

import com.example.dbProject.dto.ProductCreateDTO;
import com.example.dbProject.entity.Product;
import com.example.dbProject.exception.ProductNotFoundException;
import com.example.dbProject.repository.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    public ProductServiceImpl(ProductRepository repository3) {
        this.productRepository = repository3;
    }

    @Override
    public Collection<Product> getList() {
        return (Collection<Product>) productRepository.findAll();
    }

    @Override
    public Integer deleteProduct(Integer id) {
        productRepository.deleteById(id);
        return id;
    }

    @Override
    public Integer createProduct(ProductCreateDTO productCreateDTO) {
        return productRepository.save(new Product(productCreateDTO)).getId();
    }

    @Override
    public Product getProduct(Integer productId) {
        return productRepository.findById(productId)
                .orElseThrow(() -> new ProductNotFoundException(productId));
    }

}


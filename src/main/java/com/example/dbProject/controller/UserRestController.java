package com.example.dbProject.controller;

import com.example.dbProject.dto.UserCreateDTO;
import com.example.dbProject.dto.UserUpdateDTO;
import com.example.dbProject.entity.User;
import com.example.dbProject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping(value = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserRestController {

    @Autowired
    private UserService userService;

    @GetMapping
    public Collection<User> getUserList() {
        return userService.getList();
    }

    @DeleteMapping(value = "{id}")
    public Integer deleteUser(@PathVariable Integer id) {

        return userService.deleteUser(id);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public Integer createUser(@RequestBody @Validated UserCreateDTO user1) {
        return userService.createUser(user1);
    }

    @GetMapping(value = "{id}")
    public User getUser(@PathVariable Integer id) {
        return userService.getUser(id);
    }

    @PutMapping(value = "{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public User updateUser(@PathVariable("id") Integer id, @RequestBody UserUpdateDTO userDTO) {
        return userService.updateUser(id, userDTO)  ;
    }

    @PatchMapping(value = "{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public User updateUserProperty(@PathVariable("id") Integer id, @RequestBody UserUpdateDTO userUpdateDTO) {
        return userService.updateUserProperty(id, userUpdateDTO);

    }

    @DeleteMapping(value = "/{userId}/product/{productId}")
    public Integer deleteProductFromUser(@PathVariable Integer userId, @PathVariable Integer productId) {

        return userService.deleteProductFromUser(userId, productId);
    }

    @PutMapping(value = "/{userId}/product/{productId}")
    public Integer addProductFromUser(@PathVariable Integer userId, @PathVariable Integer productId) {

        return userService.addProductToUser(userId, productId);
    }

}